#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <assert.h>


int main(int argc, char *argv[]) 
{
  printf("\n\n");

  if (argc != 3) {printf("utilisation : %s port_serveur NbClients\n", argv[0]);exit(1);}

  ssize_t socketID = socket(PF_INET, SOCK_STREAM, 0);
  if (socketID == -1){perror("Serveur : pb creation socket :");exit(1);}
  struct sockaddr_in socketServeur;
  socklen_t lg1 = sizeof(struct sockaddr_in);
  socketServeur.sin_family = AF_INET;
  socketServeur.sin_addr.s_addr = INADDR_ANY; 
  socketServeur.sin_port = htons((short)atoi(argv[1]));
  if (bind(socketID, (struct sockaddr*) &socketServeur, sizeof(socketServeur)) == -1) {perror("Server bind error");exit(1);}
  if (getsockname(socketID, (struct sockaddr*) &socketServeur, &lg1) == -1) {perror("Error getsockname");exit(1);}

  const size_t nombreConnexionsMax = atoi(argv[2]);
  if(listen(socketID,nombreConnexionsMax) == -1) {perror("Pb listen : ");exit(1);}

  struct sockaddr_in sockDistante;
  socklen_t lg2 = sizeof(struct sockaddr_in);

  int running = 1;
  ssize_t receiveStatus;
  int numeroClient, temps_de_traitement, nombre_clients_traites = 0;
  ssize_t IdSocketDistante;
  
  while(running) {
    IdSocketDistante = accept(socketID,(struct sockaddr*) &sockDistante, &lg2);
    if(IdSocketDistante == -1) {perror("Pb accept : ");exit(1);}
    receiveStatus = recv(IdSocketDistante,&numeroClient,sizeof(int),0);
    if(receiveStatus == -1) {perror("Pb recv1 :");exit(1);}
    else if(receiveStatus == 0) {perror("The peer has closed its half size of the connection1 ");exit(1);}

    receiveStatus = recv(IdSocketDistante,&temps_de_traitement,sizeof(int),0);
    if(receiveStatus == -1) {perror("Pb recv2 :");exit(1);}
    else if(receiveStatus == 0) {perror("The peer has closed its half size of the connection2 ");exit(1);}

    printf("Serveur : le client numero %i a un temps de traitement de %i secondes\n\n",numeroClient, temps_de_traitement);
    nombre_clients_traites += 1;
    if(nombre_clients_traites == nombreConnexionsMax) {
      printf("Serveur : j'ai atteint le nombre max de connexions, je termine.\n");
      running = 0;
    } else {
      printf("Serveur : j'ai traité la demande du client de pid %i, je peux passer au client suivant.\n\n",numeroClient);      
    }
  }


  printf("\n\n\n");

  if(!running) printf("Serveur : je termine\n\n");
  
  return 0;
}
