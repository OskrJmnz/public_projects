#include <stdio.h> 
#include <unistd.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdlib.h>
#include <arpa/inet.h>
#include <string.h>
#include <math.h>
#include <sys/wait.h>

int main(int argc, char *argv[]) 
{
  printf("\n\n");
  if (argc != 4) {printf("utilisation : %s ip_serveur port_serveur nbClients\n", argv[0]);exit(1);} printf("\n\n");

  int nbClients = atoi(argv[3]);

  for (int i = 0; i < nbClients; ++i) {
      // Each new client gets a socket
      ssize_t socketID = socket(PF_INET, SOCK_STREAM, 0); 
      if(socketID == -1) {perror("Socket creation failed.\n");exit(1);}
      struct sockaddr_in socketDistante_serveur; 
      socklen_t lgA = sizeof(struct sockaddr_in);
      socketDistante_serveur.sin_family = AF_INET; 
      socketDistante_serveur.sin_addr.s_addr = inet_addr(argv[1]); 
      socketDistante_serveur.sin_port = htons((short)atol(argv[2]));
      ssize_t connectStatus = connect(socketID, (struct sockaddr*) &socketDistante_serveur,lgA);
      if(connectStatus == -1) {perror("Connect failed.\n");exit(1);}

      int randomWaitingTime = rand() % 5 + 1;
      printf("Client : client %i , mon traitement prendra %i secondes\n\n",i,randomWaitingTime);
      sleep(randomWaitingTime);
      if (send(socketID, &i, sizeof(int), 0) == -1) {perror("Message not sent, send1 ");exit(1);}
      if (send(socketID, &randomWaitingTime, sizeof(int), 0) == -1) {perror("Message not sent, send1 ");exit(1);}
      close(socketID);
    }
  return 0;
}
